﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

	public float movementTime;

	private Player player;
	private float yOffset;
	private float zOffset;

	void Awake(){
		player = GameObject.FindObjectOfType<Player> ();
	}

	// Use this for initialization
	void Start () {
		transform.position = new Vector3 (player.transform.position.x, player.transform.position.y + 1.6f, player.transform.position.z - 3.3f);

		yOffset = player.transform.position.y - this.transform.position.y;
		zOffset = player.transform.position.z - this.transform.position.z;
	}
	
	// Update is called once per frame
	void LateUpdate(){
		if (player.canMove) {
			Vector3 targetPosition = new Vector3 (player.transform.position.x, player.transform.position.y - yOffset, player.transform.position.z - zOffset);
			StartCoroutine (Move (targetPosition, movementTime));
		} else {
			return;
		}
	}

	IEnumerator Move (Vector3 destination, float duration){
		Vector3 currentPos = transform.position;
		float progress = 0f;
		float startTime = Time.time;

		while (progress < 1f) {
			progress = (Time.time - startTime) / duration;
			transform.position = Vector3.Lerp (currentPos, destination, progress);
			yield return null;
		}
	}
}
