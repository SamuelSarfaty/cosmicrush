﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knight : MonoBehaviour {

	public float movementTime;
	public float waitTime;
	public float waitToStart;

	private bool isMoving;
	private Player player;
	private Animator animator;
	private Grid[] gridArray;

	//Children
	private Transform knightStomp;
	private Transform knightExplosion;
	private Renderer childRenderer;
	private Collider childCollider;

	void Awake(){
		player = GameObject.FindObjectOfType<Player> ();
		gridArray = GameObject.FindObjectsOfType<Grid> ();
		animator = GetComponent<Animator> ();

		childRenderer = GetComponentInChildren<Renderer> ();
		childCollider = GetComponentInChildren<Collider> ();

		knightStomp = transform.FindChild ("Knight stomp");
		knightStomp.gameObject.SetActive (false);

		knightExplosion = transform.FindChild ("Knight explosion");
		knightExplosion.gameObject.SetActive (false);
	}


	// Use this for initialization
	void Start () {
		isMoving = true;
		StartCoroutine (WaitToStartMoving (waitToStart));
	}
	
	// Update is called once per frame
	void Update () {
		transform.LookAt (player.transform);

		if (!isMoving) {
			isMoving = true;
			SelectGrid ();
		}
			
	}

	public void SelectGrid(){
		int randomGrid = Random.Range (0, gridArray.Length - 1);
		Vector3 targetPosition = gridArray [randomGrid].transform.position;
		if (targetPosition != transform.position && gridArray[randomGrid].isGridRed == false) {
			animator.SetTrigger ("jump");
			StartCoroutine (MoveToGrid (targetPosition, movementTime));
		} else {
			SelectGrid ();
		}
	}

	public void Die(){
		StartCoroutine (DeathSequence ());
	}

	IEnumerator MoveToGrid(Vector3 destination, float duration){
		Vector3 currentPos = transform.position;
		float progress = 0f;
		float startTime = Time.time;

		while (progress < 1f) {
			progress = (Time.time - startTime) / duration;
			transform.position = Vector3.Lerp (currentPos, destination, progress);
			yield return null;
		}
		yield return new WaitForSeconds (waitTime);
		isMoving = false;
	}

	IEnumerator WaitToStartMoving(float secondsToStart){
		yield return new WaitForSeconds (secondsToStart);
		isMoving = false;
	}

	IEnumerator Stomp (){
		knightStomp.gameObject.SetActive (true);
		yield return new WaitForSeconds (0.8f); //duration set as slightly lower than duration of particle effect
		knightStomp.gameObject.SetActive(false);
	}

	IEnumerator DeathSequence(){
		childRenderer.enabled = false;
		childCollider.enabled = false;
		knightExplosion.gameObject.SetActive (true);
		ParticleSystem particles = knightExplosion.GetComponent<ParticleSystem> ();
		particles.Play ();
		yield return new WaitForSeconds (1.9f); //Important to kill the knight before it starts moving again
		this.gameObject.SetActive (false);
	}



}
