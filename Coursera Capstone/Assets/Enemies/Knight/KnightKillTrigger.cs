﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnightKillTrigger : MonoBehaviour {

	private Knight parent;

	void Awake(){
		parent = transform.parent.GetComponent<Knight> ();
	}

	void OnTriggerEnter(Collider other){
		if (other.gameObject.GetComponent<Player> ()) {
			Player player = other.gameObject.GetComponent<Player> ();
			player.Die ();
		}
		if (other.GetComponent<Grid> ()) {
			Grid grid = other.GetComponent<Grid> ();
			if (grid.isGridBlue) {
				parent.Die ();
			}
		}
	}
}
