﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Queen : MonoBehaviour {

	[HideInInspector]
	public bool wasKilled = false;

	private List<Grid> gridList = new List<Grid> ();
	private Grid[] gridArray;
	private Player player;

	private Transform queenExplosion;
	private Transform electricity;
	private Renderer myRenderer;

	void Awake(){
		queenExplosion = transform.FindChild ("Queen explosion");
		queenExplosion.gameObject.SetActive (false);

		electricity = transform.FindChild ("Electricity");
		electricity.gameObject.SetActive (false);

		player = GameObject.FindObjectOfType<Player> ();
		myRenderer = GetComponent<Renderer> ();
	}

	void Start(){
		gridArray = GameObject.FindObjectsOfType<Grid> ();
		foreach (Grid grid in gridArray) {
			if (grid.tag != "DoNotDestroy") {
				gridList.Add (grid);
			}
		}
	}

	public void RemoveRandomGrid(){

		if (gridList.Count >= 1) {
			int randomGrid = Random.Range (0, gridList.Count);
			gridList [randomGrid].TurnGridRed ();
			gridList.RemoveAt (randomGrid);
		}
	} 

	public void RemoveSpecificGrid(Grid grid){
		int gridToRemove = gridList.IndexOf (grid);
		gridList.RemoveAt (gridToRemove);
	}

	public void ShowElectricty(){
		StartCoroutine (ActivateElectricity ());
	}

	void OnTriggerEnter(Collider other){
		if (other.GetComponent<Player> ()) {
			StartCoroutine (Collect ());
		}
	}

	IEnumerator Collect(){
		wasKilled = true;
		myRenderer.enabled = false;
		queenExplosion.gameObject.SetActive (true);
		ParticleSystem particles = queenExplosion.GetComponent<ParticleSystem> ();
		gridList.Clear ();
		particles.Play ();
		yield return new WaitForSeconds (1.9f); // wait for particles to disappear
		Destroy (this.gameObject);
	}

	IEnumerator ActivateElectricity(){
		electricity.gameObject.SetActive (true);
		yield return new WaitForSeconds (player.movementTime);
		electricity.gameObject.SetActive (false);

	}
		
}
