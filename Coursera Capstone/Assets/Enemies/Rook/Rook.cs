﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rook : MonoBehaviour {

	[Tooltip("Checkpoint 0 is the starting point")]
	public Transform[] checkpoints;
	public float movementTime;
	public float timeToHoldAtPosition;

	private bool isMoving = false;
	private int currentTarget;

	// Use this for initialization
	void Start () {
		currentTarget = 1;
	}
		
	
	// Update is called once per frame
	void Update () {
		if (!isMoving) {
			isMoving = true;
			StartCoroutine (GoToNextPosition (checkpoints[currentTarget].transform.position, movementTime));
		}
	}

	void OnTriggerEnter(Collider other){
		if (other.GetComponent<Player> ()) {
			Player player = other.GetComponent<Player> ();
			player.Die ();
		}
	}

	IEnumerator GoToNextPosition (Vector3 destination, float duration){
		yield return new WaitForSeconds (timeToHoldAtPosition);
		Vector3 currentPos = transform.position;
		float progress = 0f;
		float startTime = Time.time;

		while (progress < 1f) {
			progress = (Time.time - startTime) / duration;
			transform.position = Vector3.Lerp (currentPos, destination, progress);
			yield return null;
		}
		if (currentTarget < checkpoints.Length - 1) {
			currentTarget++;
		} else {
			currentTarget = 0;
		}
		isMoving = false;


	}
}
