using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

	//Public variables
	[HideInInspector]
	public bool canMove;
	public float movementTime;
	public int powerCount;
	public int trapsCount;

	//Objects
	private Queen queen;
	private GameManager gm;
	private Text powerCounterText;
	private Text trapsCounterText;

	//Components and children
 	private MeshRenderer myRenderer;
	private Collider myCollider;
	private PowerSpiral powerSpiral;
	private TrapSpiral trapSpiral;
	private Transform playerExplosion;

	void Awake(){
		powerCount = 0;
		canMove = true;

		gm = GameObject.FindObjectOfType<GameManager> ();

		myRenderer = GetComponent<MeshRenderer> ();
		myCollider = GetComponent<Collider> ();

		playerExplosion = transform.FindChild ("Player explosion");
		playerExplosion.gameObject.SetActive (false);

		powerSpiral = GameObject.FindObjectOfType<PowerSpiral> ();
		powerSpiral.gameObject.SetActive (false);

		trapSpiral = GameObject.FindObjectOfType<TrapSpiral> ();
		trapSpiral.gameObject.SetActive (false);

	}
	// Use this for initialization
	void Start () {

		if (GameObject.Find ("Power counter")) {
			powerCounterText = GameObject.Find ("Power counter").GetComponent<Text> ();
			UpdatePowerCounter ();
		}

		if (GameObject.Find ("Traps counter")) {
			trapsCounterText = GameObject.Find ("Traps counter").GetComponent<Text> ();
			UpdateTrapsCounter ();

		}

		if (GameObject.FindObjectOfType<Queen> ()) {
			queen = GameObject.FindObjectOfType<Queen> ();
		} 
	}
	
	// Update is called once per frame
	void Update () {
		Movement ();
	}

	void Movement (){
		if (canMove) {
			if (Input.GetKeyDown (KeyCode.LeftArrow)) {
				Vector3 destination = transform.position + new Vector3 (-1, 0f, 0f);
				if (Physics.CheckSphere (destination, 0.2f)) {
					StartCoroutine (Move (destination, movementTime));
				}

			} else if (Input.GetKeyDown (KeyCode.RightArrow)) {
				Vector3 destination = transform.position + new Vector3 (1, 0f, 0f);
				if (Physics.CheckSphere (destination, 0.2f)) {
					StartCoroutine (Move (destination, movementTime));
				}

			} else if (Input.GetKeyDown (KeyCode.UpArrow)) {
				Vector3 destination = transform.position + new Vector3 (0f, 0f, 1);
				if (Physics.CheckSphere (destination, 0.2f)) {
					StartCoroutine (Move (destination, movementTime));
				}

			} else if (Input.GetKeyDown (KeyCode.DownArrow)) {
				Vector3 destination = transform.position + new Vector3 (0f, 0f, -1);
				if (Physics.CheckSphere (destination, 0.2f)) { 
					StartCoroutine (Move (destination, movementTime));
				}
			}
		}
	}


	void OnTriggerEnter (Collider other){
		if (other.GetComponent<Bishop> ()) {
			Bishop powerSource = other.GetComponent<Bishop> ();
			powerCount += powerSource.amountOfPower;
			powerSpiral.gameObject.SetActive (true);
			UpdatePowerCounter ();
		}

		if (other.GetComponent<BlueBishop> ()) {
			BlueBishop trapSource = other.GetComponent<BlueBishop> ();
			trapsCount += trapSource.traps;
			trapSpiral.gameObject.SetActive (true);
			UpdateTrapsCounter ();
		}
	}

	public void UpdatePowerCounter(){
		powerCounterText.text = powerCount.ToString ();
		if (powerCount <= 0) {
			powerSpiral.gameObject.SetActive (false);
		}
	}

	public void UpdateTrapsCounter(){
		trapsCounterText.text = trapsCount.ToString ();
		if (trapsCount <= 0) {
			trapSpiral.gameObject.SetActive (false);
		}
	}

	public void Die(){
		StartCoroutine (DeathSequence());
	}
		


	IEnumerator Move (Vector3 destination, float duration){
		if (queen && !queen.wasKilled) {
			queen.ShowElectricty();
		}
		canMove = false;
		Vector3 currentPos = transform.position;
		float progress = 0f;
		float startTime = Time.time;

		while (progress < 1f) {
			progress = (Time.time - startTime) / duration;
			transform.position = Vector3.Lerp (currentPos, destination, progress);
			yield return null;

		}
		if (queen) {
			queen.RemoveRandomGrid ();
		}
		canMove = true;


	}

	IEnumerator DeathSequence(){
		myRenderer.enabled = false;
		myCollider.enabled = false;
		playerExplosion.gameObject.SetActive (true);
		ParticleSystem particles = playerExplosion.GetComponent<ParticleSystem> ();
		particles.Play ();
		yield return new WaitForSeconds (1.8f);
		gm.LoadLevel ("3.1 Lose");

	}
		
}
