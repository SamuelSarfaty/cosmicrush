﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerSpiral : MonoBehaviour {

	private Player player;

	void Awake(){
		player = GameObject.FindObjectOfType<Player> ();
	}
	
	// Update is called once per frame
	void Update () {
		transform.RotateAround (player.transform.position, Vector3.up, 600 * Time.deltaTime);
	}
}
