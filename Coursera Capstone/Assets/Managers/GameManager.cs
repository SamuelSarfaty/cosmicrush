﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public static int lastLevelLoaded;
	public float timeToLoadNextLevel;
	public GameObject sceneryPrefab;

	private int loadedLevel;

	void Awake(){

		loadedLevel = SceneManager.GetActiveScene().buildIndex;

		if (GameObject.FindObjectOfType<Scenery> ()) {
			return;
		} else {
			Instantiate (sceneryPrefab);
		}

	}

	public void LoadLevel (string levelname){
		SceneManager.LoadScene (levelname);
	}

	public void LoadPuzzleLevel(int levelIndex){
		SceneManager.LoadScene (levelIndex);
		lastLevelLoaded = levelIndex;

	}

	public void LoadNextLevel(){
		lastLevelLoaded = loadedLevel + 1;
		PlayerPrefsManager.SetLastDifficulty (lastLevelLoaded);
		StartCoroutine (NextLevelSequence ());
	}

	IEnumerator NextLevelSequence(){
		yield return new WaitForSeconds (timeToLoadNextLevel);
		SceneManager.LoadScene (loadedLevel + 1);
	}

	public void LoadLastLevel(){
		SceneManager.LoadScene (lastLevelLoaded);
	}

	public void ResetProgress(){
		PlayerPrefsManager.ResetLastDifficulty ();
	}

	public void Quit(){
		Application.Quit ();
	}


}	
