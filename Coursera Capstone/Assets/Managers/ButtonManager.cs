﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour {

	public Button[] buttonArray;
	int lastLevel;

	// Use this for initialization
	void Start () {

		if (PlayerPrefsManager.GetLastDifficulty () == 0) {
			lastLevel = 3;
		} else {
			lastLevel = PlayerPrefsManager.GetLastDifficulty ();
		}

		for (int i = 0; i < buttonArray.Length; i++) {
			if (i + 3 > lastLevel) {
				buttonArray [i].interactable = false;
			}
		}

	}
		
}
