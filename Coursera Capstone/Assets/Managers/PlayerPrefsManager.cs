﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerPrefsManager {

	const string LAST_DIFFICULTY_KEY = "last_difficulty";

	public static void SetLastDifficulty(int lastDifficulty){
		PlayerPrefs.SetInt (LAST_DIFFICULTY_KEY, lastDifficulty);
	}

	public static int GetLastDifficulty(){
		return PlayerPrefs.GetInt (LAST_DIFFICULTY_KEY);

	}

	public static void ResetLastDifficulty(){
		PlayerPrefsManager.SetLastDifficulty (3);
	}

}
