﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TipManager : MonoBehaviour {

	public float timeToStart;
	public float timeToStay;

	[Tooltip("Provide tip object, not text!")]
	public GameObject[] tips;

	// Use this for initialization
	void Start () {

			foreach (GameObject tip in tips) {
				tip.SetActive (false);
			}

			StartCoroutine (ShowTips (tips.Length));
	}

	IEnumerator ShowTips(int numberOfTips){
		yield return new WaitForSeconds (timeToStart);
		for (int i = 0; i < numberOfTips; i++) {
			tips [i].SetActive (true);
			yield return new WaitForSeconds (timeToStay);
			tips [i].SetActive (false);
		}
	}
	

}
