﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructionsManager : MonoBehaviour {

	public GameObject[] instructions;

	//public float waitTime;

	void Awake(){
		foreach (GameObject instruction in instructions) {
			instruction.SetActive (true);
		}
	}

	// Use this for initialization
	void Start () {
		//StartCoroutine (DisplayInstructions (waitTime));
	}

	IEnumerator DisplayInstructions(float waitTime){
		for (int i = 0; i < instructions.Length; i++) {
			yield return new WaitForSeconds (waitTime);
			instructions [i].SetActive (true);
		}
	}

}
