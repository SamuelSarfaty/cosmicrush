﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour {

	[HideInInspector]
	public bool isGridRed, isGridWhite, isGridGreen, isGridBlue;
	public Material[] gridMaterials;
	public bool isGridTaken, turnRedOnStart;


	private Player player;
	private Queen queen;
	private Renderer myRenderer;
	private enum GridColors {White, Red, Green, Blue};

	void Awake(){
		isGridTaken = false;
	}

	// Use this for initialization
	void Start () {
		player = GameObject.FindObjectOfType<Player> ();
		myRenderer = GetComponent<Renderer> ();

		if (GameObject.FindObjectOfType<Queen> ()) {
			queen = GameObject.FindObjectOfType<Queen> ();
		}

		if (turnRedOnStart) {
			TurnGridRed ();
		} else {
			isGridWhite = true;
		}

	}

	void OnTriggerEnter (Collider other){
		if (other.GetComponent<Player> () && isGridRed && player.powerCount <= 0) {
			player.Die ();
		} else if (other.GetComponent<Player>() && isGridRed && player.powerCount >= 1) {
			player.powerCount--;
			player.UpdatePowerCounter ();
		}

		if (other.GetComponent<Player>() && queen && this.tag != "DoNotDestroy" && isGridWhite) {
			queen.RemoveSpecificGrid (this);
		}
	}

	void OnTriggerStay(Collider other){
		isGridTaken = true;
		if (other.gameObject.GetComponent<Player> ()) {
			TurnGridGreen ();
		}
	}

	void OnTriggerExit (Collider other){
		isGridTaken = false;
		if (other.GetComponent<Player> ()) {
			TurnGridRed ();
		} 


	}

	void OnMouseDown(){
		if ((player.trapsCount > 0) && isGridWhite) {
			TurnGridBlue ();
			player.trapsCount--; 
			player.UpdateTrapsCounter ();

		}
	}

	Material ChooseMaterial (GridColors color){ //This method pairs each material of the gridMaterials array to the names of GridColors enum
		switch (color)
		{
		case GridColors.White:
			return gridMaterials [0];
		case GridColors.Green:
			return gridMaterials [1];
		case GridColors.Red:
			return gridMaterials [2];
		case GridColors.Blue:
			return gridMaterials [3];
		default:
			Debug.LogWarning ("Material not specified");
			return null;
		}
			
	}

	void SwitchBools (GridColors gridColor){
		switch (gridColor) 
		{
		case GridColors.Red:
			isGridRed = true;
			isGridGreen = false;
			isGridWhite = false;
			isGridBlue = false;
			break;
		case GridColors.Green:
			isGridRed = false;
			isGridGreen = true;
			isGridWhite = false;
			isGridBlue = false;
			break;
		case GridColors.White:
			isGridRed = false;
			isGridGreen = false;
			isGridWhite = true;
			isGridBlue = false;
			break;
		case GridColors.Blue:
			isGridRed = false;
			isGridGreen = false;
			isGridWhite = false;
			isGridBlue = true;
			break;
		default:
			Debug.Log ("Bool not specified");
			break;
		}

	}
		
	public void TurnGridRed (){
		myRenderer.material = ChooseMaterial (GridColors.Red);
		SwitchBools (GridColors.Red);
	}

	public void TurnGridWhite(){
		myRenderer.material = ChooseMaterial (GridColors.White);
		SwitchBools (GridColors.White);
	}

	public void TurnGridBlue(){
		myRenderer.material = ChooseMaterial (GridColors.Blue);
		SwitchBools (GridColors.Blue);
	}

	public void TurnGridGreen(){
		myRenderer.material = ChooseMaterial (GridColors.Green);
		SwitchBools (GridColors.Green);
	}
}
