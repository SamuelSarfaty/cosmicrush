﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridManager : MonoBehaviour {

	public static int takenPawns;
	public static int remainingPawns;

	private Text pawnCounter;
	private GameManager gm;
	private Player player;
	private Pawn[] pawns;


	void Awake(){
		takenPawns = 0;
		remainingPawns = 0;

		player = GameObject.FindObjectOfType<Player> ();
	}

	// Use this for initialization
	void Start () {

		pawns = GameObject.FindObjectsOfType<Pawn> ();
		foreach (Pawn pawn in pawns) {
			remainingPawns++;
		}

		gm = GameObject.FindObjectOfType<GameManager> ();
		pawnCounter = GameObject.Find ("Pawn counter").GetComponent<Text> ();
		UpdatePawnCounter ();
	}

	void Update(){
		if (takenPawns == remainingPawns) {
			player.canMove = false;
			gm.LoadNextLevel ();
		}
	}

	public void UpdatePawnCounter(){
		pawnCounter.text = takenPawns.ToString () + " / " + remainingPawns.ToString ();
	}
			
}
