﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawn : MonoBehaviour {

	private GridManager gridManager;
	private Transform pawnExplosion;
	private MeshRenderer myRenderer;
	private Collider myCollider;

	void Awake(){
		gridManager = GameObject.FindObjectOfType<GridManager> ();
		pawnExplosion = transform.FindChild ("Pawn Explosion");
		pawnExplosion.gameObject.SetActive (false);

		myRenderer = GetComponent<MeshRenderer> ();
		myCollider = GetComponent<Collider> ();
	}

	void OnTriggerEnter(Collider other){
		if (other.GetComponent<Player>()) {
			StartCoroutine (Collect ());
		}
	}

	IEnumerator Collect(){
		GridManager.takenPawns++;
		gridManager.UpdatePawnCounter ();
		myRenderer.enabled = false;
		myCollider.enabled = false;
		pawnExplosion.gameObject.SetActive (true);
		ParticleSystem particles = pawnExplosion.GetComponent<ParticleSystem> ();
		particles.Play ();
		yield return new WaitForSeconds (1.8f); // Set it to slightly smaller than the duration of the particle animation
		this.gameObject.SetActive(false);

	}
}
