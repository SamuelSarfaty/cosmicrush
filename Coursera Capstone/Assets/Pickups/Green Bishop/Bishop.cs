﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bishop : MonoBehaviour {

	public int amountOfPower;

	private Transform explosion;
	private Transform particleEffect;
	private Transform powerNumber;
	private Renderer myRenderer;

	void Awake(){
		myRenderer = GetComponent<Renderer> ();

		powerNumber = transform.FindChild ("Power number");
		particleEffect = transform.FindChild ("Particle effect");
		explosion = transform.FindChild ("Bishop explosion");
		explosion.gameObject.SetActive (false);
	}

	void Start(){
		TextMesh numberText = powerNumber.GetComponent<TextMesh> ();
		numberText.text = amountOfPower.ToString ();
	}

	void OnTriggerEnter(Collider other){
		if (other.GetComponent<Player> ()) {
			StartCoroutine (Collect ());
		}
	}

	IEnumerator Collect(){
		myRenderer.enabled = false;
		powerNumber.gameObject.SetActive (false);
		particleEffect.gameObject.SetActive (false);
		explosion.gameObject.SetActive (true);
		yield return new WaitForSeconds (1.4f); // Set it to slightly smaller than the duration of the particle animation
		this.gameObject.SetActive(false);

	}


}
